* [pkgs.alpinelinux.org](https://pkgs.alpinelinux.org/packages?name=openrc)

### License
* BSD 2-Clause "Simplified" License
* [gitweb.gentoo.org](https://gitweb.gentoo.org/proj/openrc.git/tree/LICENSE)
* [github.com](https://github.com/OpenRC/openrc/blob/master/LICENSE)

### Sub Packages
* openrc-dev
* openrc-doc

## Services which can be added with openrc
* [postgresql](https://pkgs.alpinelinux.org/packages?name=postgresql&arch=x86_64)
  * https://gitlab.com/apk-packages-demo/postgresql
* [rsyslog](https://pkgs.alpinelinux.org/packages?name=rsyslog&arch=x86_64)
  * https://gitlab.com/apk-packages-demo/rsyslog